#ifndef UTIL_H
#define UTIL_H

#include <QString>

QByteArray getOutputConsole(const QString& arg);
QString getLineOfString(const QString& str, const int number);

#endif // UTIL_H

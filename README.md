# InternalEye

This is a fork of HardInfo - system monitor with GUI for Linux. HardInfo developed with GTK and C, and i want to port it to C++ and Qt5. 

At first, i will save all functionality of hardinfo, and after that will add some new features : thermal control, new benchmarks, new database of hardware, etc.

If you have any issues, pls tell me about that.

Thanks.